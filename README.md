# pile-bot

At a command line:
``` bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" # install brew
brew install python # pip included 
pip install virtualenv
git clone https://bitbucket.org/c-hb/pile-bot
cd pile-bot
virtualenv .env 
. .env/bin/activate 
pip install -r req.txt 
python bot.py # fill in config.py
```


