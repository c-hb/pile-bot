import praw, config, db, random, time

def login():
	return praw.Reddit(username=config.username,
				password=config.password,
				client_id=config.client_id,
				client_secret=config.client_secret,
				user_agent=config.user_agent)

def run(r, sub=config.sub):
	for cmt in r.subreddit(sub).comments(limit=25):
		if config.key_word in cmt.body.lower() and not db.cmt_exists(cmt.id) and cmt.author.name != config.username:
			print cmt.body
			db.inc_pile_height(config.key_word)
			db.add_cmt(cmt.id)
			cmt.reply("The {} pile is now {} meters tall.".format(config.key_word, db.get_pile_height(config.key_word)))
	time.sleep(10)

def main():
	db.create()
	r = login()
	while True:
		try:
			run(r)
		except KeyboardInterrupt:
			raise
		# except:
		# 	pass

if __name__=='__main__':
	main()







