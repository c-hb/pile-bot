import sqlite3, config

conn = sqlite3.connect(config.db_name)
c = conn.cursor()

def create():
	c.execute('create table if not exists piles (id integer primary key autoincrement, name text, height real)')
	c.execute('create table if not exists cmts (id integer primary key autoincrement, cmt_id text)')
	save()

def reset_tables():
	del_tables()
	create()
	save()

def del_tables():
	tables = [t[0] for t in get_tables()]
	for t in tables:
		print t
		if t != "sqlite_sequence":
			c.execute('drop table if exists {}'.format(t))
	save()

def reset_piles(init_height=config.init_height):
	c.execute('update piles set height = ?', (init_height,))
	save()

def save():
	try:
		conn.commit()
	except:
		conn.rollback()

def add_cmt(id):
	try:
		c.execute('insert into cmts(cmt_id) values(?)', (id,))
		save()
	except:
		create()

def add_pile(name, init_height=config.init_height):
	try:
		c.execute('insert into piles(name, height) values(?, ?)', (name, init_height))
		save()
	except: 
		create()
		add_pile()

def inc_pile_height(name, inc=config.inc):
	if not pile_exists(name):
		add_pile(name)
	c.execute('update piles set height = height + ? where name = ?', (inc, name))
	save()

def get_pile_height(name):
	c.execute('select height from piles where name = ?', (name,))
	return c.fetchall()[0][0]

def pile_exists(name):
	c.execute('select * from piles where name like ?', (name,))
	return len(c.fetchall()) > 0

def cmt_exists(id):
	c.execute('select * from cmts where cmt_id like ?', (id,))
	return len(c.fetchall()) > 0

def table_exists(name):
	table = c.execute("select name from sqlite_master where type='table' and name=?", (name,))
	return table.fetchone() is not None

def get_cmts():
	try:
		return c.execute('select * from cmts')
	except:
		return None

def get_piles():
	try:
		return c.execute('select * from piles')
	except:
		return None

def get_tables():
	try:
		return c.execute("select name from sqlite_master where type='table' order by name")
	except:
		return None

def list():
	if get_tables() is not None:
		print get_tables().fetchall()
	if get_piles() is not None:
		print get_piles().fetchall()
	if get_cmts() is not None:
		print get_cmts().fetchall()
